resource "aws_iam_role" "kube-external-dns" {
  name               = "kube-external-dns"
  assume_role_policy = data.aws_iam_policy_document.eks-assume-role-policy.json
}

data "aws_iam_policy_document" "kube-external-dns_document" {
  statement {
    actions = [
      "route53:ChangeResourceRecordSets",
    ]

    resources = [
      "arn:aws:route53:::hostedzone/*",
    ]
  }

  statement {
    actions = [
      "route53:ListHostedZones",
      "route53:ListResourceRecordSets",
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_policy" "kube-external-dns" {
  name = "kube-external-dns"

  policy = data.aws_iam_policy_document.kube-external-dns_document.json
}

resource "aws_iam_role_policy_attachment" "kube-external-dns" {
  role       = aws_iam_role.kube-external-dns.name
  policy_arn = aws_iam_policy.kube-external-dns.arn
}
