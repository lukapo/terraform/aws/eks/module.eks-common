# EKS-COMMON MODULE

## Input Variables
* `arns` (required)

## Output Values
* `aws_iam_role_alb_ingress_controller_arn`
* `aws_iam_role_cluster_autoscaler_arn`
* `aws_iam_role_kube_external_dns_arn`

## Managed Resources
* `aws_iam_policy.alb-ingress-controller` from `aws`
* `aws_iam_policy.cluster-autoscaler` from `aws`
* `aws_iam_policy.kube-external-dns` from `aws`
* `aws_iam_role.alb-ingress-controller` from `aws`
* `aws_iam_role.cluster-autoscaler` from `aws`
* `aws_iam_role.kube-external-dns` from `aws`
* `aws_iam_role_policy_attachment.alb-ingress-controller` from `aws`
* `aws_iam_role_policy_attachment.cluster-autoscaler` from `aws`
* `aws_iam_role_policy_attachment.kube-external-dns` from `aws`

## Data Resources
* `data.aws_iam_policy_document.alb-ingress-controller_document` from `aws`
* `data.aws_iam_policy_document.cluster-autoscaler_document` from `aws`
* `data.aws_iam_policy_document.eks-assume-role-policy` from `aws`
* `data.aws_iam_policy_document.kube-external-dns_document` from `aws`
