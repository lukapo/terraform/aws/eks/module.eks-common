output "aws_iam_role_alb_ingress_controller_arn" {
  value = aws_iam_role.alb-ingress-controller.arn
}

output "aws_iam_role_kube_external_dns_arn" {
  value = aws_iam_role.kube-external-dns.arn
}

output "aws_iam_role_cluster_autoscaler_arn" {
  value = aws_iam_role.cluster-autoscaler.arn
}
