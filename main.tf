data "aws_iam_policy_document" "eks-assume-role-policy" {
  statement {
    effect = "Allow"

    principals {
      type = "Service"

      identifiers = [
        "ec2.amazonaws.com",
      ]
    }

    principals {
      type = "AWS"

      identifiers = var.arns
    }

    actions = [
      "sts:AssumeRole",
    ]
  }
}
